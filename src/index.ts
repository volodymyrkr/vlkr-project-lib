class HelloWorldLib {
    helloFrom(name: string): string {
        const greeting = `Hello my dear, ${name}`;
        console.log(greeting)
        return greeting;
    }
}

export const helloWorld = new HelloWorldLib();
